**PLANT CARE ASSISTANT**

A plant care assistant is a user friendly Interface designed to help individuals care for their plants by reminding them of important tasks such as watering.
The core feature of a plant care assistant is its notification system. Users can set up watering reminder and provide guidelines for plant growth based on its category.

**INTRODUCTION**

A simple yet effective tool designed to help you keep your plants healthy and thriving with timely watering reminder.This Interface focuses on one key aspect of plant care: ensuring your plants receive the hydration they need to flourish.
With our notification-based system, you can say goodbye to forgetting to water your plants.
You'll receive gentle reminder via notification when it's time to water each of your plants. No more guesswork or forgetting to check on your green friends.

**Features**

Specify Plant Category:
Easily input the category of each of your plants into the GUI, so you can keep track of them individually.

Specify Plant Name:
Easily input the name of each of your plants into the GUI, so you can keep track of them individually.

Built-in Timer:
Utilize the integrated timer feature to ensure you water your plants for the right amount of time. This helps prevent over or under-watering, promoting optimal plant health.

Guidelines:
Access comprehensive guidelines tailored to each plant category's unique requirements. From sunlight exposure to soil type, this GUI provides essential care instructions to help you nurture your plants effectively.

**Dependencies**


List of Software,libraries,or tools that this project depends on 

**Python**: 3.6 or higher

**Pillow**: 10.2.0

**pip**: 24.0

**tk** 0.1.0

**DB Browser for SQlite**: 3.12.2

**Installation**

To set up the project locally,follow these steps:

**clone the repository:**

https://gitlab.com/wise_svecw/project1.git

**Navigate to the project directory:**

```cd project1```

**Now the project is set up,you can run it with the following command:(python file_name.py)**

```py main.py```
