from tkinter import ttk,Tk, Frame, Label, Entry, Button, messagebox,OptionMenu,StringVar,font
from PIL import Image,ImageTk
import sqlite3
import tkinter as tk
# from plyer import notification
from tkinter import messagebox
import datetime,time
import threading
import pygame
pygame.mixer.init()

root = Tk()
font_lbl  = ('Courier New',18, 'bold')
root.title('Plant-Care Assitant')
root.geometry('700x560+350+120')

class Home:
    def __init__(self, root):
        self.root = root
        root.iconbitmap("../assets/logo.ico")
        self.root.resizable(False,False)
        self.home_page()

        conn = sqlite3.connect('signup.db')
        cursor = conn.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS users(username TEXT NOT NULL, password TEXT PRIMARY KEY)")
        conn.close()

    def home_page(self):
        self.user_frame = Frame(root,width = 740,height = 560,bg = 'white')
        self.user_frame.place(x = 0,y = 0)

        
        self.label_name = Label(text  = 'PLANT CARE ASSISTANT ', font = font_lbl, bg = 'white',fg = 'green')
        self.label_name.place(x = 200, y = 50)

        self.user_logo = Image.open('../assets/plat.png')
        self.user_logo = self.user_logo.resize((740,560))

        self.user_logo = ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl = Label(self.user_frame,image = self.user_logo)
        self.user_logo_lbl.place(x = 0, y = -20)

        
        self.user_btn = Button(self.user_frame,text = 'START', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command = self.user_signup)
        self.user_btn.place(x = 500, y = 500)
 
    def user_signup(self):
        self.root = root
        self.user_frame.destroy()

        self.account_frame = Frame(root,width = 700,height = 600,bg = "antiquewhite1")
        self.account_frame.place(x = 0,y = 0)

    
        self.leaf_logo = Image.open('../assets/tree.png')
        self.leaf_logo = self.leaf_logo.resize((360,590))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.account_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 360, y = 0)

        self.user_logo_1 = Image.open('../assets/logo1.png')
        self.user_logo_1 = self.user_logo_1.resize((100,70))

        self.user_logo_1 = ImageTk.PhotoImage(self.user_logo_1)
        self.user_logo_lbl_1 = Label(self.account_frame,image = self.user_logo_1)
        self.user_logo_lbl_1.place(x = 150, y = 10)
    
        self.title_name = Label(text = 'Create your Account',font = font_lbl,bg = 'antiquewhite1',fg = 'black')
        self.title_name.place(x = 50,y = 90)

        self.user_name = Label(self.account_frame, text  = 'USER NAME ', font = font_lbl, bg = 'antiquewhite1', fg = 'black')
        self.user_name.place(x = 10, y = 150)
        self.user_name_entry = Entry(self.account_frame, width = 18)
        self.user_name_entry.place(x = 180, y = 155)

        self.user_pass = Label(self.account_frame, text = 'PASSWORD', font = font_lbl, bg = 'antiquewhite1', fg = 'black', width = 10)
        self.user_pass.place(x = 10, y = 200)
        self.user_pass_entry = Entry(self.account_frame, width = 18, show = '*')
        self.user_pass_entry.place(x = 180, y = 205)

        self.login_btn = Button(self.account_frame,text = 'Back', font = font_lbl,bg = 'coral',fg = 'black',width = 10,command =self.home_page )
        self.login_btn.place(x = 20, y = 325)

        self.user_btn = Button(self.account_frame,text = 'Sign up', font = font_lbl,bg = 'coral',fg = 'black',width = 10,command =self.register_user )
        self.user_btn.place(x = 200, y = 325)

        self.already = Label(self.account_frame, text = "Already have an account?",font=("Arial", 10),bg = 'antiquewhite1',fg = 'black')
        self.already.place(x =40, y = 400)
        # self.login_btn = Button(self.account_frame,text = 'Log in', font = font_lbl,bg = 'antiquewhite1',fg = 'white',width = 10,command =self.login )
        self.link_button = Button(self.account_frame, text="login", fg="brown", font=("Arial", 10, "underline"), bd=0, cursor="hand2", command=self.login)
        self.link_button.pack()
        self.link_button.place(x = 200, y = 400)
       

    def register_user(self):
        name = self.user_name_entry.get()
        password = self.user_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("Signup.db")
            cursor = conn.cursor()
            cursor.execute('SELECT username FROM users WHERE username=?', [name])
            if cursor.fetchone() is not None:
                messagebox.showerror("Error", "Username already exists.")
            else:
                cursor.execute('INSERT INTO users VALUES (?,  ?)', [name, password])
                conn.commit()
                conn.close()
                messagebox.showinfo("Success", "Account has been created.")
                self.create_page()
        else:
            messagebox.showerror("Error", "Enter all data.")

    def login(self):
        self.root = root
        self.account_frame.destroy()

        self.login_frame = Frame(root,width = 740,height = 560,bg = 'antiquewhite1')
        self.login_frame.place(x = 0,y = 0)

        self.leaf_logo = Image.open('../assets/tree.png')
        self.leaf_logo = self.leaf_logo.resize((360,590))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.login_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 360, y = 0)

        self.user_logo_2 = Image.open('../assets/logo1.png')
        self.user_logo_2 = self.user_logo_2.resize((100,100))

        self.user_logo_2 = ImageTk.PhotoImage(self.user_logo_2)
        self.user_logo_lbl_2 = Label(self.login_frame,image = self.user_logo_2)
        self.user_logo_lbl_2.place(x = 150, y = 10)

        self.title_name = Label(text = 'LOG IN',font = font_lbl,bg = 'antiquewhite1',fg = 'black')
        self.title_name.place(x = 135,y = 125)

        self.user_name = Label(self.login_frame, text  = 'USER NAME ', font = font_lbl, bg = 'antiquewhite1', fg = 'black')
        self.user_name.place(x = 20, y = 220)
        self.luser_name_entry = Entry(self.login_frame, width = 15)
        self.luser_name_entry.place(x = 220, y = 225)

        self.user_pass = Label(self.login_frame, text = 'PASSWORD', font = font_lbl, bg = 'antiquewhite1', fg = 'black', width = 10)
        self.user_pass.place(x = 20, y = 270)
        self.luser_pass_entry = Entry(self.login_frame, width = 15, show = '*')
        self.luser_pass_entry.place(x = 220, y = 275)

        self.user_btn = Button(self.login_frame,text = 'Log in', font = font_lbl,bg = 'coral',fg = 'black',width = 10,command =self.verify_login)
        self.user_btn.place(x = 200, y = 390)

        self.back_btn = Button(self.login_frame,text = 'Back', font = font_lbl,bg = 'coral',fg = 'black',width = 10,command =self.user_signup)
        self.back_btn.place(x = 30, y = 390)
        
    def verify_login(self):
        name = self.luser_name_entry.get()
        password = self.luser_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("signup.db")
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM users WHERE username=? AND password=?', (name, password))
            user = cursor.fetchone()
            conn.close()
            if user:
                messagebox.showinfo("Success", f"Welcome, {name}!")
                self.create_page()
            else:
                messagebox.showerror("Error", "Invalid username or password.")
        else:
            messagebox.showerror("Error", "Enter both username and password.")
     
    def create_page(self):
        self.root = root
        self.login_frame.destroy()
        self.create_frame = Frame(root,width = 740,height = 560,bg = "grey")
        self.create_frame.place(x = 0,y=0)

        self.leaf_logo = Image.open('../assets/bd_img.png')
        self.leaf_logo = self.leaf_logo.resize((740,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.create_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 0)

        self.label_name = Label(self.create_frame,text  = 'Plant Details ', font = font_lbl, bg = 'darkkhaki',fg = 'white')
        self.label_name.place(x = 240, y = 40)

        self.user_logo_1 = Image.open('../assets/logo1.png')
        self.user_logo_1 = self.user_logo_1.resize((100,70))

        self.user_logo_1 = ImageTk.PhotoImage(self.user_logo_1)
        self.user_logo_lbl_1 = Label(self.create_frame,image = self.user_logo_1)
        self.user_logo_lbl_1.place(x = 300, y = 100)
    
        
        self.Plant_name = Label(self.create_frame, text  = 'Plant category', font = font_lbl, bg = 'darkkhaki', fg = 'white')
        self.Plant_name.place(x = 185, y = 240)
        

        plant_categories = ["Flowers", "Herbs", "Indoor","Fruits","Veggies","Climbers"]

        self.selected_category = StringVar()


        # Create the dropdown menu
        self.search_entry = OptionMenu(self.create_frame, self.selected_category, *plant_categories)
        self.search_entry.place(x = 400, y = 243)

        self.Plant_cate = Label(self.create_frame, text  = 'Plant Name ', font = font_lbl, bg = 'darkkhaki', fg = 'white')
        self.Plant_cate.place(x = 185, y = 310)
        self.Plant_cate_entry = Entry(self.create_frame, width = 20)
        self.Plant_cate_entry.place(x = 400, y = 313)


        plus_label = Button(self.create_frame, text="Save", font=("Arial", 20),bg='darkolivegreen3',width='5',command=self.save_plant)
        plus_label.place(x = 180, y = 380)

        plus_label = Button(self.create_frame, text="Next", font=("Arial", 20),bg='darkolivegreen3',width='5',command=self.add_remainder)
        plus_label.place(x = 300, y = 380)

        self.logout_btn = Button(self.create_frame,text = "Logout",font = ("Arial", 20),bg = 'darkolivegreen3',command=self.user_signup)
        self.logout_btn.place(x = 420,y = 380)

       
        self.link_button = Button(self.create_frame, text="View Saved Plant Details", fg="black", font=("Arial", 12, "underline"), bd=0, cursor="hand2",bg="#E3CF57",command=self.plant_details)
        self.link_button.pack()
        self.link_button.place(x = 300, y = 460)

    def save_plant(self):
        category = self.selected_category.get()
        plant_name = self.Plant_cate_entry.get().strip()

        if not plant_name:
            messagebox.showerror("Error", "Please enter plant name.")
            return

        conn = sqlite3.connect("plant_info.db")
        cursor = conn.cursor()
        
        # Check if the plant already exists
        cursor.execute("SELECT * FROM plants WHERE name=? AND category=?", (plant_name, category))
        existing_plant = cursor.fetchone()
        if existing_plant:
            messagebox.showerror("Error", "This plant already exists.")
        else:
            # Insert the plant details into the database
            cursor.execute("INSERT INTO plants (name, category) VALUES (?, ?)", (plant_name, category))
            conn.commit()
            conn.close()
            messagebox.showinfo("Success", "Plant details saved successfully.")

    def delete_plant(self):

        self.root = root
        self.plant_details_frame.destroy()
        self.delete_frame = Frame(root,width = 740,height = 560,bg = "grey")
        self.delete_frame.place(x = 0,y=0)

        self.leaf_logo = Image.open('../assets/bd_img.png')
        self.leaf_logo = self.leaf_logo.resize((740,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.delete_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 0)

        self.user_logo_1 = Image.open('../assets/logo1.png')
        self.user_logo_1 = self.user_logo_1.resize((100,70))

        self.user_logo_1 = ImageTk.PhotoImage(self.user_logo_1)
        self.user_logo_lbl_1 = Label(self.delete_frame,image = self.user_logo_1)
        self.user_logo_lbl_1.place(x = 300, y = 100)
    
        
        self.Plant_name = Label(self.delete_frame, text  = 'Plant category', font = font_lbl, bg = 'darkkhaki', fg = 'white')
        self.Plant_name.place(x = 185, y = 240)
        

        plant_categories = ["Flowers", "Herbs", "Indoor","Fruits","Veggies","Climbers"]

        self.selected_category = StringVar()


        # Create the dropdown menu
        self.search_entry = OptionMenu(self.delete_frame, self.selected_category, *plant_categories)
        self.search_entry.place(x = 400, y = 243)

        self.Plant_cate = Label(self.delete_frame, text  = 'Plant Name ', font = font_lbl, bg = 'darkkhaki', fg = 'white')
        self.Plant_cate.place(x = 185, y = 310)
        self.Plant_cate_entry = Entry(self.delete_frame, width = 20)
        self.Plant_cate_entry.place(x = 400, y = 313)


        self.plant_name = self.Plant_cate_entry.get().strip()
        self.category = self.selected_category.get().strip()

        self.delete_btn = Button(self.delete_frame,text = "Delete",font = ("Arial", 20),bg = 'darkolivegreen3',command=self.delete)
        self.delete_btn.place(x = 190,y = 410)


        self.back_btn = Button(self.delete_frame,text = "Back",font = ("Arial", 20),bg = 'darkolivegreen3',command=self.plant_details)
        self.back_btn.place(x = 350,y = 410)

    def delete(self):
        plant_name = self.Plant_cate_entry.get().strip()
        category = self.selected_category.get().strip()
        if not plant_name or not category:
            messagebox.showerror("Error", "Please enter both plant name and category.")
            return

        conn = sqlite3.connect("plant_info.db")
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM plants WHERE name=? AND category=?", (plant_name, category))
        existing_plant = cursor.fetchone()

        if existing_plant:
            # Delete the plant from the database
            cursor.execute("DELETE FROM plants WHERE name=? AND category=?", (plant_name,category))
            conn.commit()
            conn.close()
            messagebox.showinfo("Success", "Plant details deleted successfully.")
        else:
            messagebox.showerror("Error", "Plant not found in the database.")

    def plant_details(self):
        self.root = root
        self.create_frame.destroy()

        self.plant_details_frame = Frame(root, width=740, height=560, bg="grey")
        self.plant_details_frame.place(x=0, y=0)

        self.leaf_logo = Image.open('../assets/bd_img.png')
        self.leaf_logo = self.leaf_logo.resize((740, 560))
        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.plant_details_frame, image=self.leaf_logo)
        self.leaf_logo_lbl.place(x=0, y=0)

        self.search_label = Label(self.plant_details_frame , text="Search Plant Category:",font = ('Courier New',12, 'bold'), bg = 'darkkhaki',fg = 'white')
        self.search_label.place(x = 50 , y = 120)
        
        
        plant_categories = ["Flowers", "Herbs", "Indoor","Fruits","Veggies","Climbers"]

        self.selected_category = StringVar()
        self.search_entry = OptionMenu(self.plant_details_frame, self.selected_category, *plant_categories)
        self.search_entry.place(x = 320, y = 120)

        self.search_btn = Button(self.plant_details_frame,text = "Search",font = ("Arial", 12),bg = 'darkolivegreen3',command=self.search_plant)
        self.search_btn.place(x = 420,y = 120)
        
        self.result_label = Label(self.plant_details_frame, text="Plant Info",font = ('Courier New',20, 'bold'), bg = 'darkkhaki',fg = 'white')
        self.result_label.place(x = 300, y = 40)
        
        self.result_text = tk.Text(self.plant_details_frame, width=20, height=10)
        self.result_text.place(x = 280, y = 180)
        
        self.link_button = Button(self.plant_details_frame, text="Delete Saved Plant Details", fg="black", font=("Arial", 12, "underline"), bd=0, cursor="hand2",bg="#E3CF57",command=self.delete_plant)
        self.link_button.pack()
        self.link_button.place(x = 160, y = 460)

        self.notify_button = Button(self.plant_details_frame, text="Back",font = ("Arial", 20),bg = 'darkolivegreen3',command = self.create_page )
        self.notify_button.place(x = 400, y = 440)
        self.result_text.config(state='disabled')

    def search_plant(self):
        category_name = self.selected_category.get().strip()
        self.result_text.config(state='normal')
        self.result_text.delete('1.0', tk.END)
        self.conn = sqlite3.connect("plant_info.db")
        self.cursor = self.conn.cursor()
        if category_name:
            self.cursor.execute("SELECT name FROM plants WHERE category=?", (category_name,))
            results = self.cursor.fetchall()
            if results:
                for result in results:
                    self.result_text.insert(tk.END, result[0] + "\n")  # Display each plant name on a new line
                # Increase font size
                self.result_text.tag_configure("big", font=("Helvetica", 14))
                self.result_text.tag_add("big", "1.0", "end")
            else:
                self.result_text.insert(tk.END, "No plants found in this category.")
        else:
            self.result_text.insert(tk.END, "Please select a plant category.")
        self.result_text.config(state='disabled')

    def add_remainder(self):
        self.root = root
        self.create_frame.destroy()
        self.remainder_frame = Frame(root,width = 740,height = 560,bg = "grey")
        self.remainder_frame.place(x = 0,y=0)

        self.leaf_logo = Image.open('../assets/bd_img.png')
        self.leaf_logo = self.leaf_logo.resize((740,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.remainder_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 0)

        self.label_name = Label(self.remainder_frame,text  = 'Reminder', font = font_lbl, bg = 'darkkhaki',fg = 'white')
        self.label_name.place(x = 260, y = 40)

        self.message_label = Label(self.remainder_frame, text  = 'Message', font = font_lbl, bg = 'darkkhaki', fg = 'white',width='16')
        self.message_label.place(x = 170, y = 180)
        self.message_entry = Entry(self.remainder_frame, width = 20)
        self.message_entry.place(x = 435, y = 183)

        self.time_label = Label(self.remainder_frame, text  = 'Time(HH:MM)', font = font_lbl, bg = 'darkkhaki', fg = 'white',width='16')
        self.time_label.place(x = 170, y = 270)
        self.time_entry = Entry(self.remainder_frame, width = 20)
        self.time_entry.place(x = 435, y = 273)


        self.logout_btn = Button(self.remainder_frame,text = "Back",font = ("Arial", 20),bg = 'darkolivegreen3',command=self.create_page)
        self.logout_btn.place(x = 170,y = 460)

        self.remainder_label = Button(self.remainder_frame, text="Guidelines", font=("Arial", 20),bg='darkolivegreen3',width='8',command=self.create_gui)
        self.remainder_label.place(x = 305, y = 460)

        self.remainder_label = Button(self.remainder_frame, text="Notify Me", font=("Arial", 20),bg='darkolivegreen3',width='7',command = self.notify_me)
        self.remainder_label.place(x = 500, y = 460)
        

    def notify_me(self):
        message = self.message_entry.get()
        time_str = self.time_entry.get()

        try:
            time_obj = datetime.datetime.strptime(time_str, "%H:%M").time()
        except ValueError:
            messagebox.showerror("Error", "Invalid time format")
            return
        today_date = datetime.date.today()
        reminder_time = datetime.datetime.combine(today_date, time_obj)
        current_time = datetime.datetime.now()

        

        if reminder_time <= current_time:
            messagebox.showwarning("Warning", "Cannot set reminder for past time or day")
            return

        time_diff = (reminder_time - current_time).total_seconds()
        threading.Timer(time_diff, self.show_notification, args=[message]).start()

        messagebox.showinfo("Success", "Reminder set successfully")
        

    def show_notification(self,message):
        while True:
            current_time = datetime.datetime.now().time()
            reminder_time = datetime.datetime.strptime(self.time_entry.get(), "%H:%M").time()

            if current_time.hour == reminder_time.hour and current_time.minute == reminder_time.minute:
                pygame.mixer.music.load("../assets/notification.wav")
                pygame.mixer.music.play()
                messagebox.showinfo("Reminder", message)
            time.sleep(24 * 60 * 60) 
        pygame.mixer.music.load("../assets/notification.wav")
        pygame.mixer.music.play()
        messagebox.showinfo("Reminder", message)
        
    def create_gui(self):
       
        self.root = root
        self.remainder_frame.destroy()

        self.gui_frame = Frame(root,width = 740,height = 560,bg = "grey")
        self.gui_frame.place(x = 0,y=0)

        self.leaf_logo = Image.open('../assets/bd_img.png')
        self.leaf_logo = self.leaf_logo.resize((740,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.gui_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 0)

        self.search_label = Label(self.gui_frame, text="Search Plant Category:",font = ('Courier New',12, 'bold'), bg = 'darkkhaki',fg = 'white')
        self.search_label.place(x = 50 , y = 100)
        
        
        
        plant_categories = ["Flowers", "Herbs", "Indoor","Fruits","Veggies","Climbers"]

        self.selected_category = StringVar()
        
        self.search_entry = OptionMenu(self.gui_frame, self.selected_category, *plant_categories)
        self.search_entry.place(x = 320, y = 100)

        self.search_btn = Button(self.gui_frame,text = "Search",font = ("Arial", 12),bg = 'darkolivegreen3',command=self.search_category)
        self.search_btn.place(x = 420,y = 100)

        
        
        self.result_label = Label(self.gui_frame, text="Guidelines",font = font_lbl, bg = 'darkkhaki',fg = 'white')
        self.result_label.place(x = 320, y = 20)
        
        self.result_text = tk.Text(self.gui_frame, width=75, height=20)
        self.result_text.place(x = 80, y = 140)

       
        
        self.notify_button = Button(self.gui_frame, text="Back",font = ("Arial", 20),bg = 'darkolivegreen3', command = self.add_remainder )
        self.notify_button.place(x = 400, y = 490)
        self.result_text.config(state='disabled')
        
    def search_category(self):

        category_name = self.selected_category.get().strip()
        self.result_text.config(state='normal')
        self.result_text.delete('1.0', tk.END)
        self.conn = sqlite3.connect("plant_info.db")
        self.cursor = self.conn.cursor()
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS guidelines (
                                    id INTEGER PRIMARY KEY,
                                    category TEXT NOT NULL,
                                    guideline TEXT NOT NULL
                        )''')
        if category_name:
            self.cursor.execute("SELECT guideline FROM guidelines WHERE category=?", (category_name,))
            result = self.cursor.fetchone()
            if result:
                custom_font = font.Font(family="Arial",size=11)
                self.result_text.configure(font=custom_font)
                self.result_text.insert(tk.END, result[0])
            else:
                self.result_text.insert(tk.END, "Plant not found.")
        else:
            self.result_text.insert(tk.END, "Please enter a plant name.")
        self.result_text.config(state='disabled')

home = Home(root)
root.mainloop()
